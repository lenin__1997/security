package com.codeboard.tryone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDTO login(UserDTO userData) {
		UserDTO user = userRepository.findByUsername(userData.getUsername());
		return user;
	}

}
