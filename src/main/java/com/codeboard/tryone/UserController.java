package com.codeboard.tryone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController  {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/hello")
	@PreAuthorize("hasAuthority('ROLE_hero')")
	public String hello() {
		return "Hello World";
	}
	
	@PostMapping("/login")
	public ResponseEntity<UserDTO> login(@RequestBody UserDTO userData ) {
	UserDTO user = userService.login(userData);
	System.out.println(user);
	if(user.getPassword().equals(userData.getPassword())) {
		return ResponseEntity.ok(user);
	}
	return (ResponseEntity<UserDTO>)ResponseEntity.internalServerError();
	
	}
}
