package com.codeboard.tryone.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfigurer {

	@Autowired
	private DataSource dataSource;

//	@Bean
//	public UserDetails userDetailsService(PasswordEncoder encoder) {
////		UserDetails user = User.withUsername("user").password(encoder.encode("zxy")).roles("user").build();
//		UserDTO user = new UserDTO();
//		UserDetails user1 = User.withUsername(user.getUsername()).password(encoder.encode(user.getPassword())).roles("user").build() ;
//		return user1;
//	}
	@Autowired
	protected void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().passwordEncoder(new BCryptPasswordEncoder()).dataSource(dataSource)
				.usersByUsernameQuery("select username,password from userdto where username = ?")
				.authoritiesByUsernameQuery("select username, role from userdto where username=?");
	}
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().authenticated().and().formLogin().permitAll().and().logout()
				.permitAll(false);

	}

//	@Bean
//	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
//		return http.csrf().disable()
//					.authorizeHttpRequests()
//					.requestMatchers("/login").permitAll().and()
//					.authorizeHttpRequests()
//					.requestMatchers("/hello").authenticated().and()
//					.formLogin().and().build();
//	}
//	
//	@Bean
//	public PasswordEncoder passwordEncoder() {
//		return new BCryptPasswordEncoder();
//	}

}
