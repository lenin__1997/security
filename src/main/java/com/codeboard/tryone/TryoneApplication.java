package com.codeboard.tryone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TryoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(TryoneApplication.class, args);
	}

}
